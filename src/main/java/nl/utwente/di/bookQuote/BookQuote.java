package nl.utwente.di.bookQuote;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class BookQuote extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Quoter quoter;
	
    public void init() throws ServletException {
    	quoter = new Quoter();
        quoter.init();
    }
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Book Quote";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>ISBN number: " +
                   request.getParameter("isbn") + "\n" +
                "  <P>Price: " +
                   quoter.getBookPrice(request.getParameter("isbn")) +
                "</BODY></HTML>");
  }

  public static class Quoter {
      Map<String, Double> booknumMapping = new HashMap<String, Double>();

      public void init(){
          booknumMapping.put("1", 10.0);
          booknumMapping.put("2", 45.0);
          booknumMapping.put("3", 20.0);
          booknumMapping.put("4", 35.0);
          booknumMapping.put("5", 50.0);
          booknumMapping.put("others", 0.0);
      }

       public double getBookPrice(String isbn){
           if (booknumMapping.containsKey(isbn)) {
               return booknumMapping.get(isbn);
           } else {
               return booknumMapping.get("others");
           }
       }
  }
}
